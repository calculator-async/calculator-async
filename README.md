# calculator-async-api

Async-API standard is deffined [here](https://www.asyncapi.com/).

## Studio

[Online tool](https://studio.asyncapi.com/?url=https://raw.githubusercontent.com/asyncapi/asyncapi/v2.2.0/examples/simple.yml#operation-subscribe-calculator/compute) for test your yaml

## How to configure

- [schema](https://www.asyncapi.com/docs/specifications/v2.4.0)
- [channel](https://www.asyncapi.com/docs/specifications/v2.4.0#channelsObject)
- [bindings](https://github.com/asyncapi/bindings/blob/master/amqp/README.md#channel)

## Generator

### Node

> npm install -g @asyncapi/generator

> ag asyncapi.yaml @asyncapi/python-paho-template -o asyncapi-example

### Docker

> docker run --rm -it -v ${PWD}/example:/app/example \

> asyncapi/generator asyncapi.yaml @asyncapi/python-paho-template -o asyncapi-example
